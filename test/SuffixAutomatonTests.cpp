#include <gtest/gtest.h>

#include <SuffixAutomaton.hpp>

#define CHECK_ALL_SUFFIX(automaton, string)                           \
  for (size_t i = 0; i < string.size(); ++i) {                        \
    EXPECT_TRUE(automaton.IsSuffix(string.substr(i, string.size()))); \
  }

#define CHECK_ALL_NON_SUFFIX(automaton, string)                  \
  for (size_t i = 0; i < string.size() - 1; ++i) {               \
    for (size_t j = i + 1; j < string.size() - 1; ++j) {         \
      EXPECT_FALSE(automaton.IsSuffix(string.substr(i, j - i))); \
    }                                                            \
  }

template <typename T>
using MyMap = std::map<size_t, T>;

TEST(SuffixAutomaton, TheSimpliestString) {
  std::string s = "ab";
  auto automaton = SuffixAutomaton<char, MyMap>(s);

  CHECK_ALL_SUFFIX(automaton, s)

  CHECK_ALL_NON_SUFFIX(automaton, s)
}

TEST(SuffixAutomaton, SimpleString) {
  std::string s = "abc";
  auto automaton = SuffixAutomaton<char, MyMap>(s);

  CHECK_ALL_SUFFIX(automaton, s)

  CHECK_ALL_NON_SUFFIX(automaton, s)
}

TEST(SuffixAutomaton, BigString) {
  std::string s = "abcdefg";
  auto automaton = SuffixAutomaton<char, MyMap>(s);

  CHECK_ALL_SUFFIX(automaton, s)

  CHECK_ALL_NON_SUFFIX(automaton, s)
}

TEST(SuffixAutomaton, MonoString) {
  std::string s = "aaa";
  auto automaton = SuffixAutomaton<char, MyMap>(s);

  CHECK_ALL_SUFFIX(automaton, s)
}

TEST(SuffixAutomaton, ThirdCase) {
  std::string s = "abbab";
  auto automaton = SuffixAutomaton<char, MyMap>(s);

  CHECK_ALL_SUFFIX(automaton, s)

  EXPECT_FALSE(automaton.IsSuffix(std::string("aba")));
  EXPECT_FALSE(automaton.IsSuffix(std::string("a")));
  EXPECT_FALSE(automaton.IsSuffix(std::string("ba")));
}

TEST(SuffixAutomaton, Monstrosity) {
  std::string s = "abbabdccacbaaabbccfcaaaccaaabbccabcbabcccaaa";
  auto automaton = SuffixAutomaton<char, MyMap>(s);

  CHECK_ALL_SUFFIX(automaton, s)
}