#include <gtest/gtest.h>

#include <ZFunction.hpp>

TEST(PrefixFunction, SimpleString) {
  std::string s = "ababa";
  auto zeta = ZetFunction(s.begin(), s.end());

  EXPECT_EQ(zeta[0], 0);
  EXPECT_EQ(zeta[1], 0);
  EXPECT_EQ(zeta[2], 3);
  EXPECT_EQ(zeta[3], 0);
  EXPECT_EQ(zeta[4], 1);
}

TEST(ZFunction, LongString) {
  std::string s = "abdcabadabdcabd";
  auto zeta = ZetFunction(s.begin(), s.end());

  EXPECT_EQ(zeta[0], 0);
  EXPECT_EQ(zeta[1], 0);
  EXPECT_EQ(zeta[2], 0);
  EXPECT_EQ(zeta[3], 0);
  EXPECT_EQ(zeta[4], 2);
  EXPECT_EQ(zeta[5], 0);
  EXPECT_EQ(zeta[6], 1);
  EXPECT_EQ(zeta[7], 0);
  EXPECT_EQ(zeta[8], 6);
  EXPECT_EQ(zeta[9], 0);
  EXPECT_EQ(zeta[10], 0);
  EXPECT_EQ(zeta[11], 0);
  EXPECT_EQ(zeta[12], 3);
  EXPECT_EQ(zeta[13], 0);
  EXPECT_EQ(zeta[14], 0);
}

TEST(ZFunction, TrickyString) {
  std::string s = "abcabcabcabd";
  auto zeta = ZetFunction(s.begin(), s.end());

  EXPECT_EQ(zeta[0], 0);
  EXPECT_EQ(zeta[1], 0);
  EXPECT_EQ(zeta[2], 0);
  EXPECT_EQ(zeta[3], 8);
  EXPECT_EQ(zeta[4], 0);
  EXPECT_EQ(zeta[5], 0);
  EXPECT_EQ(zeta[6], 5);
  EXPECT_EQ(zeta[7], 0);
  EXPECT_EQ(zeta[8], 0);
  EXPECT_EQ(zeta[9], 2);
  EXPECT_EQ(zeta[10], 0);
  EXPECT_EQ(zeta[11], 0);
}

TEST(ZFunction, ContestString) {
  std::string s = "abracadabra";
  auto zeta = ZetFunction(s.begin(), s.end());

  EXPECT_EQ(zeta[0], 0);
  EXPECT_EQ(zeta[1], 0);
  EXPECT_EQ(zeta[2], 0);
  EXPECT_EQ(zeta[3], 1);
  EXPECT_EQ(zeta[4], 0);
  EXPECT_EQ(zeta[5], 1);
  EXPECT_EQ(zeta[6], 0);
  EXPECT_EQ(zeta[7], 4);
  EXPECT_EQ(zeta[8], 0);
  EXPECT_EQ(zeta[9], 0);
  EXPECT_EQ(zeta[10], 1);
}
