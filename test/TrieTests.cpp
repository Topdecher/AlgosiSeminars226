#include <gtest/gtest.h>

#include <Trie.hpp>

template <typename T>
using MyMap = std::map<size_t, T>;

TEST(Trie, Simple) {
  auto trie = Trie<char, MyMap, EnglishAlphabet>();

  EXPECT_FALSE(trie.HasWord(std::string("abacaba")));

  trie.AddWord(std::string("abacaba"));
  EXPECT_TRUE(trie.HasWord(std::string("abacaba")));
}

template <typename T>
using MyUnorderedMap = std::unordered_map<size_t, T>;

TEST(Trie, NotSimple) {
  auto trie = Trie<char, MyUnorderedMap, EnglishAlphabet>();

  EXPECT_FALSE(trie.HasWord(std::string("abacaba")));
  EXPECT_FALSE(trie.HasWord(std::string("aba")));

  trie.AddWord(std::string("abacaba"));
  EXPECT_TRUE(trie.HasWord(std::string("abacaba")));
  EXPECT_FALSE(trie.HasWord(std::string("aba")));

  trie.AddWord(std::string("aba"));
  EXPECT_TRUE(trie.HasWord(std::string("abacaba")));
  EXPECT_TRUE(trie.HasWord(std::string("aba")));
}

template <typename T>
using MyArray = std::array<T, EnglishAlphabet::Size()>;

TEST(Trie, Dificult) {
  auto trie = Trie<char, MyArray, EnglishAlphabet>();

  EXPECT_FALSE(trie.HasWord(std::string("abacaba")));
  EXPECT_FALSE(trie.HasWord(std::string("aba")));
  EXPECT_FALSE(trie.HasWord(std::string("baka")));

  trie.AddWord(std::string("abacaba"));
  EXPECT_TRUE(trie.HasWord(std::string("abacaba")));
  EXPECT_FALSE(trie.HasWord(std::string("aba")));
  EXPECT_FALSE(trie.HasWord(std::string("baka")));

  trie.AddWord(std::string("aba"));
  EXPECT_TRUE(trie.HasWord(std::string("abacaba")));
  EXPECT_TRUE(trie.HasWord(std::string("aba")));
  EXPECT_FALSE(trie.HasWord(std::string("baka")));

  trie.AddWord(std::string("baka"));
  EXPECT_TRUE(trie.HasWord(std::string("abacaba")));
  EXPECT_TRUE(trie.HasWord(std::string("aba")));
  EXPECT_TRUE(trie.HasWord(std::string("baka")));
}
