#include <gtest/gtest.h>

#include <PrefixFunction.hpp>

TEST(PrefixFunction, SimpleString) {
  std::string s = "ababa";
  auto pref = Prefix(s.begin(), s.end());

  EXPECT_EQ(pref[0], 0);
  EXPECT_EQ(pref[1], 0);
  EXPECT_EQ(pref[2], 1);
  EXPECT_EQ(pref[3], 2);
  EXPECT_EQ(pref[4], 3);
}

TEST(PrefixFunction, LongString) {
  std::string s = "abdcabadabdcabd";
  auto pref = Prefix(s.begin(), s.end());

  EXPECT_EQ(pref[0], 0);
  EXPECT_EQ(pref[1], 0);
  EXPECT_EQ(pref[2], 0);
  EXPECT_EQ(pref[3], 0);
  EXPECT_EQ(pref[4], 1);
  EXPECT_EQ(pref[5], 2);
  EXPECT_EQ(pref[6], 1);
  EXPECT_EQ(pref[7], 0);
  EXPECT_EQ(pref[8], 1);
  EXPECT_EQ(pref[9], 2);
  EXPECT_EQ(pref[10], 3);
  EXPECT_EQ(pref[11], 4);
  EXPECT_EQ(pref[12], 5);
  EXPECT_EQ(pref[13], 6);
  EXPECT_EQ(pref[14], 3);
}

TEST(PrefixFunction, TrickyString) {
  std::string s = "abcabcabcabd";
  auto pref = Prefix(s.begin(), s.end());

  EXPECT_EQ(pref[0], 0);
  EXPECT_EQ(pref[1], 0);
  EXPECT_EQ(pref[2], 0);
  EXPECT_EQ(pref[3], 1);
  EXPECT_EQ(pref[4], 2);
  EXPECT_EQ(pref[5], 3);
  EXPECT_EQ(pref[6], 4);
  EXPECT_EQ(pref[7], 5);
  EXPECT_EQ(pref[8], 6);
  EXPECT_EQ(pref[9], 7);
  EXPECT_EQ(pref[10], 8);
  EXPECT_EQ(pref[11], 0);
}

TEST(PrefixFunction, ContestString) {
  std::string s = "abracadabra";
  auto pref = Prefix(s.begin(), s.end());

  EXPECT_EQ(pref[0], 0);
  EXPECT_EQ(pref[1], 0);
  EXPECT_EQ(pref[2], 0);
  EXPECT_EQ(pref[3], 1);
  EXPECT_EQ(pref[4], 0);
  EXPECT_EQ(pref[5], 1);
  EXPECT_EQ(pref[6], 0);
  EXPECT_EQ(pref[7], 1);
  EXPECT_EQ(pref[8], 2);
  EXPECT_EQ(pref[9], 3);
  EXPECT_EQ(pref[10], 4);
}
