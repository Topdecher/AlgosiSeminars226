#include <gtest/gtest.h>

#include <Treap.hpp>

#include "CheckOutput.hpp"

// TEST(PrintVisitor, Float)
// {
//     checkOutput([]() {
//         auto test = ASTNodeVariant(Float(13.4));
//         printASTNode(test);
//     }, "Float 13.4\n");
// }

using Params = std::tuple<int64_t, std::monostate, int64_t>;

TEST(Treap, Building) {
  std::vector<Params> data;

  data.reserve(7);
  data.emplace_back(0, std::monostate{}, 5);
  data.emplace_back(1, std::monostate{}, 3);
  data.emplace_back(2, std::monostate{}, 2);
  data.emplace_back(3, std::monostate{}, 9);
  data.emplace_back(4, std::monostate{}, 11);
  data.emplace_back(5, std::monostate{}, 4);
  data.emplace_back(6, std::monostate{}, 6);
  Treap<int64_t, std::monostate> curevo{std::span<const Params>(data.begin(), data.end())};
  checkOutput([&]() { curevo.Print(); }, "2 0 0\n3 1 0\n0 2 6\n6 0 5\n4 0 0\n3 4 7\n6 0 0\n");
}

TEST(Treap, Merge) {
  std::vector<Params> data1;

  data1.reserve(3);
  data1.emplace_back(0, std::monostate{}, 5);
  data1.emplace_back(1, std::monostate{}, 3);
  data1.emplace_back(2, std::monostate{}, 2);

  std::vector<Params> data2;

  data2.reserve(4);
  data2.emplace_back(3, std::monostate{}, 9);
  data2.emplace_back(4, std::monostate{}, 11);
  data2.emplace_back(5, std::monostate{}, 4);
  data2.emplace_back(6, std::monostate{}, 6);

  Treap<int64_t, std::monostate> curevo1{std::span<const Params>(data1.begin(), data1.end())};
  Treap<int64_t, std::monostate> curevo2{std::span<const Params>(data2.begin(), data2.end())};

  // curevo1.PrintRaw();
  // std::cout << "\n";
  // curevo2.PrintRaw();
  // std::cout << "\n";

  auto curevo = Merge(std::move(curevo1), std::move(curevo2));

  // curevo.PrintRaw();

  checkOutput(
      [&]() {
        curevo.Print();
        ;
      },
      "2 0 0\n3 1 0\n0 2 6\n6 0 5\n4 0 0\n3 4 7\n6 0 0\n");

  EXPECT_TRUE(curevo1.IsEmpty());
  EXPECT_TRUE(curevo2.IsEmpty());
}

TEST(Treap, Split) {
  std::vector<Params> data;

  data.reserve(7);
  data.emplace_back(0, std::monostate{}, 5);
  data.emplace_back(1, std::monostate{}, 3);
  data.emplace_back(2, std::monostate{}, 2);
  data.emplace_back(3, std::monostate{}, 9);
  data.emplace_back(4, std::monostate{}, 11);
  data.emplace_back(5, std::monostate{}, 4);
  data.emplace_back(6, std::monostate{}, 6);

  Treap<int64_t, std::monostate> curevo{std::span<const Params>(data.begin(), data.end())};

  auto [curevo1, curevo2] = Split(std::move(curevo), 3);

  auto curevo_result = Merge(std::move(curevo1), std::move(curevo2));

  checkOutput(
      [&]() {
        curevo_result.Print();
        ;
      },
      "2 0 0\n3 1 0\n0 2 6\n6 0 5\n4 0 0\n3 4 7\n6 0 0\n");

  EXPECT_TRUE(curevo.IsEmpty());
  EXPECT_TRUE(curevo1.IsEmpty());
  EXPECT_TRUE(curevo2.IsEmpty());
}

TEST(Treap, Insert1) {
  std::vector<Params> data;

  data.reserve(6);
  data.emplace_back(0, std::monostate{}, 5);
  data.emplace_back(1, std::monostate{}, 3);
  data.emplace_back(2, std::monostate{}, 2);
  data.emplace_back(3, std::monostate{}, 9);
  data.emplace_back(4, std::monostate{}, 11);
  data.emplace_back(5, std::monostate{}, 4);
  // data.emplace_back(6, std::monostate{}, 6);

  Treap<int64_t, std::monostate> curevo{std::span<const Params>(data.begin(), data.end())};

  curevo.Insert(6, std::monostate{}, 6);

  checkOutput(
      [&]() {
        curevo.Print();
        ;
      },
      "2 0 0\n3 1 0\n0 2 6\n6 0 5\n4 0 0\n3 4 7\n6 0 0\n");
}

TEST(Treap, Insert2) {
  std::vector<Params> data;

  data.reserve(6);
  data.emplace_back(0, std::monostate{}, 5);
  // data.emplace_back(1, std::monostate{}, 3);
  data.emplace_back(2, std::monostate{}, 2);
  data.emplace_back(3, std::monostate{}, 9);
  data.emplace_back(4, std::monostate{}, 11);
  data.emplace_back(5, std::monostate{}, 4);
  data.emplace_back(6, std::monostate{}, 6);

  Treap<int64_t, std::monostate> curevo{std::span<const Params>(data.begin(), data.end())};

  curevo.Insert(1, std::monostate{}, 3);

  checkOutput(
      [&]() {
        curevo.Print();
        ;
      },
      "2 0 0\n3 1 0\n0 2 6\n6 0 5\n4 0 0\n3 4 7\n6 0 0\n");
}

TEST(Treap, Erase1) {
  std::vector<Params> data;

  data.reserve(7);
  data.emplace_back(0, std::monostate{}, 5);
  data.emplace_back(1, std::monostate{}, 3);
  data.emplace_back(2, std::monostate{}, 2);
  data.emplace_back(3, std::monostate{}, 9);
  data.emplace_back(4, std::monostate{}, 11);
  data.emplace_back(5, std::monostate{}, 4);
  data.emplace_back(6, std::monostate{}, 6);
  data.emplace_back(7, std::monostate{}, 20);

  Treap<int64_t, std::monostate> curevo{std::span<const Params>(data.begin(), data.end())};

  curevo.Erase(7);

  checkOutput([&]() { curevo.Print(); }, "2 0 0\n3 1 0\n0 2 6\n6 0 5\n4 0 0\n3 4 7\n6 0 0\n");
}

TEST(Treap, Erase2) {
  std::vector<Params> data;

  data.reserve(7);
  data.emplace_back(0, std::monostate{}, 5);
  data.emplace_back(1, std::monostate{}, 3);
  data.emplace_back(2, std::monostate{}, 2);
  data.emplace_back(3, std::monostate{}, 9);
  data.emplace_back(4, std::monostate{}, 11);
  data.emplace_back(5, std::monostate{}, 4);
  data.emplace_back(6, std::monostate{}, 6);
  data.emplace_back(7, std::monostate{}, 20);

  Treap<int64_t, std::monostate> curevo{std::span<const Params>(data.begin(), data.end())};

  curevo.Erase(7);
  curevo.Erase(1);

  curevo.Insert(1, std::monostate{}, 3);

  checkOutput(
      [&]() {
        curevo.Print();
        ;
      },
      "2 0 0\n3 1 0\n0 2 6\n6 0 5\n4 0 0\n3 4 7\n6 0 0\n");
}
