#!/bin/bash

set -e

TARGET="$1"

./build.sh $@

EXECUTABLE="bin/${TARGET}.out"

./$EXECUTABLE
