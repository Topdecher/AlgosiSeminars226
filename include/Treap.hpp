#include <array>
#include <iostream>
#include <optional>
#include <span>
#include <stdexcept>
#include <vector>
#include <variant>
#include <tuple>
#include <type_traits>
#include <random>

// template <typename Key, typename Value, typename Priority, typename CompareKey,
//     typename ComparePriority>
// class Treap;

// template <typename Key, typename Value, typename Priority, typename CompareKey, typename ComparePriority>
// Treap(
//     typename Treap<Key, Value, Priority, CompareKey, ComparePriority>::Node*,
//     const CompareKey&,
//     const ComparePriority&) -> Treap<Key, Value, Priority, CompareKey, ComparePriority>;

template <typename Key, typename Value, typename Priority = int64_t, typename CompareKey = std::less<Key>,
          typename ComparePriority = std::less<Priority>>
class Treap {
 public:
  //
  //  NODE
  //

  struct Node {
    const Key key;
    Value value;
    const Priority priority;

    //  NODE constructors
    Node(Key key, Value value, Priority priority) : key(key), value(value), priority(priority) {
    }

    //  NODE copy
    explicit Node(const Node& other)
        : key(other.key)
        , value(other.value)
        , priority(other.priority)
        , parent_(nullptr)
        , left_child_(new Node(*other.left_child_))
        , right_child_(new Node(*other.right_child_))
        , size_(other.size_) {
    }
    Node& operator=(Node other) {
      Swap(*this, other);
    }

    //  NODE move
    Node(Node&& other) = delete;
    Node& operator=(Node&& other) = delete;

    Node* LowerBound(Key other_key) {
      if (key < other_key) {
        if (HasRightChild()) {
          return right_child_->LowerBound(other_key);
        }
        return nullptr;
      }
      if (HasLeftChild()) {
        Node* result = left_child_->LowerBound(key);
        if (result == nullptr) {
          return this;
        }
        return result;
      }
      return this;
    }

    const Node* LowerBound(Key other_key) const {
      if (key < other_key) {
        if (HasRightChild()) {
          return right_child_->LowerBound(other_key);
        }
        return nullptr;
      }
      if (HasLeftChild()) {
        Node* result = left_child_->LowerBound(key);
        if (result == nullptr) {
          return this;
        }
        return result;
      }
      return this;
    }

    Node* UpperBound(Key other_key) {
      if (key <= other_key) {
        if (HasRightChild()) {
          return right_child_->LowerBound(other_key);
        }
        return nullptr;
      }
      if (HasLeftChild()) {
        Node* result = left_child_->LowerBound(key);
        if (result == nullptr) {
          return this;
        }
        return result;
      }
      return this;
    }

    const Node* UpperBound(Key other_key) const {
      if (key <= other_key) {
        if (HasRightChild()) {
          return right_child_->LowerBound(other_key);
        }
        return nullptr;
      }
      if (HasLeftChild()) {
        Node* result = left_child_->LowerBound(key);
        if (result == nullptr) {
          return this;
        }
        return result;
      }
      return this;
    }

    // Node* Kth(size_t k) {
    //   if (k > size_) {
    //     return nullptr;
    //   }
    //   if (HasLeftChild()) {
    //     if (left_child_->GetSize() <= k) {
    //       return left_child_->Kth(k);
    //     }
    //     return right_child_->Kth(k - left_child_->GetSize() - 1);
    //   }
    //   if (k == 0) {
    //     return this;
    //   }
    //   return right_child_->Kth(k - 1);
    // }

    // const Node* Kth(size_t k) const {
    //   if (k > size_) {
    //     return nullptr;
    //   }
    //   if (HasLeftChild()) {
    //     if (left_child_->GetSize() <= k) {
    //       return left_child_->Kth(k);
    //     }
    //     return right_child_->Kth(k - left_child_->GetSize() - 1);
    //   }
    //   if (k == 0) {
    //     return this;
    //   }
    //   return right_child_->Kth(k - 1);
    // }

    // size_t GetSize() const {
    //   return size_;
    // }

    //  parent behaviour
    Node* GetParent() const {
      return parent_;
    }
    bool HasParent() const {
      return parent_;
    }

    void RemoveParent() {
      if (HasParent()) {
        if (IsLeftChild()) {
          parent_->left_child_ = nullptr;
        } else {
          parent_->right_child_ = nullptr;
        }
      }
      parent_ = nullptr;
    }

    void SetParent(Node* parent) {
      if (parent_ == parent) {
        return;
      }
      RemoveParent();
      parent_ = parent;
    }

    //  left child behaviour
    Node* GetLeftChild() const {
      return left_child_;
    }
    bool HasLeftChild() const {
      return (left_child_ != nullptr);
    }
    bool IsLeftChild() const {
      if (parent_ == nullptr) {
        return false;
      }
      return parent_->GetLeftChild() == this;
    }
    void SetLeftChild(Node* child) {
      if (left_child_ != nullptr) {
        left_child_->RemoveParent();
        // size_ -= left_child_->GetSize() + 1;
      }

      if (child != nullptr) {

        child->SetParent(this);
        // size_ += child->GetSize() + 1;
      }
      left_child_ = child;
    }

    //  right child behaviour
    Node* GetRightChild() const {
      return right_child_;
    }
    bool HasRightChild() const {
      return (right_child_ != nullptr);
    }
    bool IsRightChild() const {
      if (parent_ == nullptr) {
        return false;
      }
      return parent_->GetRightChild() == this;
    }
    void SetRightChild(Node* child) {
      if (right_child_ != nullptr) {
        right_child_->RemoveParent();
        // size_ -= right_child_->GetSize() + 1;
      }

      if (child != nullptr) {
        child->SetParent(this);
        // size_ += child->GetSize() + 1;
      }
      right_child_ = child;
    }

    ~Node() {
      if (left_child_ != nullptr) {
        delete left_child_;
      }
      if (right_child_ != nullptr) {
        delete right_child_;
      }
    }

   private:
    Node* parent_ = nullptr;
    Node* left_child_ = nullptr;
    Node* right_child_ = nullptr;

    size_t size_ = 1;

    void Swap(Node& left, Node& right) {
      std::swap(left.key, right.key);
      std::swap(left.value, right.value);
      std::swap(left.priority, right.priority);
      std::swap(left.parent_, right.parent_);
      std::swap(left.left_child_, right.left_child_);
      std::swap(left.right_child_, right.right_child_);
      std::swap(left.size_, right.size_);
    }
  };

  //
  //  END NODE
  //

  //
  //  ITERATORS
  //

  struct Sentinel {};

  struct Iterator {
   public:
    using PtrType = Node*;

    explicit Iterator(PtrType node) : current_node_(node) {
    }

    Iterator& operator++() {
      if (current_node_ == nullptr) {
        return *this;
      }
      if (current_node_->HasRightChild()) {
        current_node_ = current_node_->GetRightChild();
        while (current_node_->HasLeftChild()) {
          current_node_ = current_node_->GetLeftChild();
        }
      } else {
        while (current_node_->IsRightChild()) {
          current_node_ = current_node_->GetParent();
        }
        current_node_ = current_node_->GetParent();
      }
      return *this;
    }
    Iterator operator++(int) {
      Iterator temp = *this;
      this->operator++();
      return temp;
    }

    Iterator& operator--() {
      if (current_node_ == nullptr) {
        return *this;
      }
      if (current_node_->HasLeftChild()) {
        current_node_ = current_node_->GetLeftChild();
        while (current_node_->HasRightChild()) {
          current_node_ = current_node_->GetRightChild();
        }
      } else {
        while (current_node_->IsLeftChild()) {
          current_node_ = current_node_->GetParent();
        }
        current_node_ = current_node_->GetParent();
      }
      return *this;
    }
    Iterator operator--(int) {
      Iterator temp = *this;
      this->operator--();
      return temp;
    }

    Node& operator*() {
      return *current_node_;
    }

    const Node& operator*() const {
      return *current_node_;
    }

    Node* operator->() {
      return this->current_node_;
    }

    const Node* operator->() const {
      return this->current_node_;
    }

    operator bool() const {  // NOLINT
      return current_node_;
    }

    friend bool operator==(const Sentinel&, const Iterator& other) {
      return other.current_node_ == nullptr;
    }
    friend bool operator==(const Iterator& other, const Sentinel&) {
      return other.current_node_ == nullptr;
    }

   private:
    PtrType current_node_;
  };

  using iterator = Iterator;        // NOLINT
  using const_iterator = Iterator;  // NOLINT

  //
  //  ITERATORS END
  //

  // TREAP constructors
  explicit Treap(Node* root = nullptr, const CompareKey& compare_key = CompareKey(),
                 const ComparePriority& compare_priority = ComparePriority())
      : root_(root), compare_key_(compare_key), compare_priority_(compare_priority) {
  }
  explicit Treap(const std::span<const std::tuple<Key, Value, Priority>>& array,
                 const CompareKey& compare_key = CompareKey(),
                 const ComparePriority& compare_priority = ComparePriority())
      : Treap(nullptr, compare_key, compare_priority) {
    Node* last_node = nullptr;
    for (const auto& [key, value, priority] : array) {
      while (last_node != nullptr && compare_priority(priority, last_node->priority)) {
        last_node = last_node->GetParent();
      }
      Node* current_node = new Node{key, value, priority};
      if (last_node == nullptr) {
        current_node->SetLeftChild(root_);
        root_ = current_node;
      } else {
        current_node->SetLeftChild(last_node->GetRightChild());
        last_node->SetRightChild(current_node);
      }
      last_node = current_node;
    }
  }

  //  TREAP move
  explicit Treap(const Treap& other)
      : root_(new Node(*other.root_)), compare_key_(other.compare_key_), compare_priority_(other.compare_priority_) {
  }
  Treap& operator=(Treap other) {
    Swap(*this, other);
    return *this;
  }

  //  TREAP move
  Treap(Treap&& other) noexcept
      : root_(other.root_), compare_key_(other.compare_key_), compare_priority_(other.compare_priority_) {
    other.root_ = nullptr;
  }

  //  container behaviour
  iterator begin() {  // NOLINT
    if (IsEmpty())
      return iterator(nullptr);

    Node* current_node = root_;
    while (current_node->HasLeftChild()) {
      current_node = current_node->GetLeftChild();
    }
    return iterator(current_node);
  }
  const_iterator begin() const {  // NOLINT
    if (IsEmpty())
      return const_iterator(nullptr);

    Node* current_node = root_;
    while (current_node->HasLeftChild()) {
      current_node = current_node->GetLeftChild();
    }
    return const_iterator(current_node);
  }
  const_iterator cbegin() const {  // NOLINT
    Node* current_node = root_;
    while (current_node->HasLeftChild()) {
      current_node = current_node->GetLeftChild();
    }
    return const_iterator(current_node);
  }
  Sentinel end() {  // NOLINT
    return Sentinel{};
  }
  Sentinel end() const {  // NOLINT
    return Sentinel{};
  }
  Sentinel cend() const {  // NOLINT
    return Sentinel{};
  }
  //  end container behaviour

  void Insert(Key key, Value value, Priority priority);
  void Erase(Key key);

  iterator LowerBound(Key key) {
    return iterator(root_->LowerBound(key));
  }

  const_iterator LowerBound(Key key) const {
    return const_iterator(root_->LowerBound(key));
  }

  iterator UpperBound(Key key) {
    return iterator(root_->UpperBound(key));
  }

  const_iterator UpperBound(Key key) const {
    return const_iterator(root_->UpperBound(key));
  }

  bool IsEmpty() const {
    return root_ == nullptr;
  }

  //   iterator Kth(size_t k) {
  //     return iterator(root_->Kth(k));
  //   }

  //   const_iterator Kth(size_t k) const {
  //     return const_iterator(root_->Kth(k));
  //   }

  void Print() const {
    for (auto&& node : *this) {
      std::cout << (node.HasParent() ? node.GetParent()->key + 1 : 0) << " "
                << (node.HasLeftChild() ? node.GetLeftChild()->key + 1 : 0) << " "
                << (node.HasRightChild() ? node.GetRightChild()->key + 1 : 0) << "\n";
    }
  }

  ~Treap() {
    if (root_ != nullptr) {
      delete root_;
    }
  }

  void PrintRaw();

 private:
  Node* root_ = nullptr;
  CompareKey compare_key_;
  ComparePriority compare_priority_;

  void Swap(Treap& left, Treap& right) {
    std::swap(left.root_, right.root_);
    std::swap(left.compare_key_, right.compare_key_);
    std::swap(left.compare_priority_, right.compare_priority_);
  }

  friend Treap Merge(Treap&& left, Treap&& right) {
    if (left.root_ == nullptr) {
      return right;
    }

    if (right.root_ == nullptr) {
      return left;
    }

    if (!left.compare_priority_(left.root_->priority, right.root_->priority)) {
      auto left_sub_tree_node = right.root_->GetLeftChild();
      right.root_->SetLeftChild(nullptr);

      auto left_sub_tree = Treap(left_sub_tree_node, right.compare_key_, right.compare_priority_);

      auto merge_result = Merge(std::move(left), std::move(left_sub_tree));
      right.root_->SetLeftChild(merge_result.root_);
      merge_result.root_ = nullptr;
      return right;
    }

    auto right_sub_tree_node = left.root_->GetRightChild();
    left.root_->SetRightChild(nullptr);

    auto right_sub_tree = Treap(right_sub_tree_node, left.compare_key_, left.compare_priority_);

    auto merge_result = Merge(std::move(right_sub_tree), std::move(right));
    left.root_->SetRightChild(merge_result.root_);
    merge_result.root_ = nullptr;
    return left;
  }

  friend std::pair<Treap<Key, Value, Priority, CompareKey, ComparePriority>,
                   Treap<Key, Value, Priority, CompareKey, ComparePriority>>
  Split(Treap<Key, Value, Priority, CompareKey, ComparePriority>&& treap, const Key& key) {
    if (treap.IsEmpty()) {
      return {Treap(nullptr), Treap(nullptr)};
    }

    if (treap.compare_key_(treap.root_->key, key)) {
      auto right_child = treap.root_->GetRightChild();
      treap.root_->SetRightChild(nullptr);

      auto [left_treap, right_treap] = Split(Treap(right_child, treap.compare_key_, treap.compare_priority_), key);
      treap.root_->SetRightChild(left_treap.root_);

      left_treap.root_ = nullptr;

      return {std::move(treap), std::move(right_treap)};
    }

    auto left_child = treap.root_->GetLeftChild();
    treap.root_->SetLeftChild(nullptr);

    auto [left_treap, right_treap] = Split(Treap(left_child, treap.compare_key_, treap.compare_priority_), key);
    treap.root_->SetLeftChild(right_treap.root_);

    right_treap.root_ = nullptr;

    return {std::move(left_treap), std::move(treap)};
  }
};

// template <typename Key, typename Value, typename Priority, typename CompareKey, typename ComparePriority>
// Treap<Key, Value, Priority, CompareKey, ComparePriority> Merge(Treap<Key, Value, Priority, CompareKey,
// ComparePriority>&& left, Treap<Key, Value, Priority, CompareKey, ComparePriority>&& right) {
//   if (left.root_ == nullptr) {
//     return right;
//   }

//   if (right.root_ == nullptr) {
//     return left;
//   }

//   if (left.compare_priority_(left.root_->priority, right.root_->priority)) {
//     auto left_sub_tree_node = right.root_->GetLeftChild();
//     right.root_->SetLeftChild(nullptr);

//     auto left_sub_tree = Treap<Key, Value, Priority, CompareKey, ComparePriority>(left_sub_tree_node,
//     right.compare_key_, right.compare_priority_);

//     auto merge_result = Merge(std::move(left), std::move(left_sub_tree));
//     right.root_->SetLeftChild(merge_result.root_);
//     merge_result.root_ = nullptr;
//     return right;
//   }

//   auto right_sub_tree_node = left.root_->GetRightChild();
//   left.root_->SetRightChild(nullptr);
//   auto right_sub_tree = Treap<Key, Value, Priority, CompareKey, ComparePriority>(right_sub_tree_node,
//   left.compare_key_, left.compare_priority_); auto merge_result = Merge(std::move(right_sub_tree), std::move(right));
//   right.root_->SetRightChild(merge_result.root_);
//   merge_result.root_ = nullptr;
//   return left;
// }

// template <typename Key, typename Value, typename Priority, typename CompareKey, typename ComparePriority>
// std::pair<Treap<Key, Value, Priority, CompareKey, ComparePriority>, Treap<Key, Value, Priority, CompareKey,
// ComparePriority> > Split(Treap<Key, Value, Priority, CompareKey, ComparePriority>&& treap, const Key& key) {
//   if (treap.root_ == nullptr) {
//     return { nullptr, nullptr };
//   }

//   if (CompareKey(treap.root_->key, key)) {
//     auto right_child = treap.root_->GetRightChild();
//     treap.root_->SetRightChild(nullptr);

//     auto [left_treap, right_treap] =
//         Split(Treap<Key, Value, Priority, CompareKey, ComparePriority>(right_child), key);
//     treap.root_->SetRightChild(left_treap.root_);

//     return { treap, right_treap };
//   }

//   auto left_child = treap.root_->GetLeftChild();
//   treap.root_->SetLeftChild(nullptr);

//   auto [left_treap, right_treap] =
//       Split(Treap<Key, Value, Priority, CompareKey, ComparePriority>(left_child), key);
//   treap.root_->SetLeftChild(right_treap.root_);

//   return { left_treap, treap };
// }

template <typename Key, typename Value, typename Priority, typename CompareKey, typename ComparePriority>
void Treap<Key, Value, Priority, CompareKey, ComparePriority>::Insert(Key key, Value value, Priority priority) {
  auto [left_tree, right_tree] = Split /*<Key, Value, Priority, CompareKey, ComparePriority>*/ (std::move(*this), key);
  Treap node_tree = Treap(new Node{key, value, priority});
  auto merge_result =
      Merge /*<Key, Value, Priority, CompareKey, ComparePriority>*/ (std::move(left_tree), std::move(node_tree));
  *this =
      Merge /*<Key, Value, Priority, CompareKey, ComparePriority>*/ (std::move(merge_result), std::move(right_tree));
}

template <typename Key, typename Value, typename Priority, typename CompareKey, typename ComparePriority>
void Treap<Key, Value, Priority, CompareKey, ComparePriority>::Erase(Key key) {
  auto [left_part, right] = Split(std::move(*this), key);

  if (++right.begin() == right.end()) {
    *this = std::move(left_part);
    return;
  }

  auto [treap_key, right_part] = Split(std::move(right), (++right.begin())->key);
  *this = Merge(std::move(left_part), std::move(right_part));
}

template <typename Key, typename Value, typename Priority, typename CompareKey, typename ComparePriority>
void Print(const Treap<Key, Value, Priority, CompareKey, ComparePriority>& treap) {
  for (auto&& node : treap) {
    std::cout << (node.HasParent() ? node.GetParent()->key + 1 : 0) << " "
              << (node.HasLeftChild() ? node.GetLeftChild()->key + 1 : 0) << " "
              << (node.HasRightChild() ? node.GetRightChild()->key + 1 : 0) << "\n";
  }
}

template <typename Key, typename Value, typename Priority, typename CompareKey, typename ComparePriority>
void PrintRaw1(typename Treap<Key, Value, Priority, CompareKey, ComparePriority>::Node* node) {
  std::cout << node->priority << " LEFT (";
  if (node->HasLeftChild()) {
    PrintRaw1<Key, Value, Priority, CompareKey, ComparePriority>(node->GetLeftChild());
  }
  std::cout << ") RIGHT (";
  if (node->HasRightChild()) {
    PrintRaw1<Key, Value, Priority, CompareKey, ComparePriority>(node->GetRightChild());
  }
  std::cout << ")";
}

template <typename Key, typename Value, typename Priority, typename CompareKey, typename ComparePriority>
void Treap<Key, Value, Priority, CompareKey, ComparePriority>::PrintRaw() {
  PrintRaw1<Key, Value, Priority, CompareKey, ComparePriority>(root_);
}

// int main() {
//   srand(0);
//   using Treap = Treap<int, std::monostate>;
//   Treap treap{};
//   std::string input_str;  // NOLINT
//   int k;                  // NOLINT
//   while (true) {
//     std::cin >> input_str >> k;
//     if (input_str == "insert") {
//       treap.Insert(k, std::monostate{}, rand());
//     } else if (input_str == "delete") {
//       treap.Erase(k);
//     } else if (input_str == "exists") {
//       if (treap.LowerBound(k)->key == k) {
//         std::cout << "true";
//       } else {
//         std::cout << "false";
//       }
//     } else if (input_str == "next") {
//       Treap::iterator iter = treap.UpperBound(k);
//       if (iter) {
//         std::cout << iter->key;
//       } else {
//         std::cout << "none";
//       }
//     } else if (input_str == "prev") {
//       Treap::iterator iter = --treap.LowerBound(k);
//       if (iter) {
//         std::cout << iter->key;
//       } else {
//         std::cout << "none";
//       }
//     } else if (input_str == "kth") {
//       Treap::iterator iter = treap.Kth(k);
//       if (iter) {
//         std::cout << iter->key;
//       } else {
//         std::cout << "none";
//       }
//     }
//   }
//   return 0;
// }
