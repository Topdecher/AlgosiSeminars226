#include <memory>
#include <vector>
#include <iostream>
#include "LexicConcepts.hpp"

template <typename T, template <typename> typename Container, Alphabet<T> ConcreteAlphabet = EnglishAlphabet>
class SuffixAutomaton {
  struct Node;

 public:
  template <Word<T> ConcreteWord = std::string>
  explicit SuffixAutomaton(const ConcreteWord& word) {
    data_.emplace_back(new Node{.is_terminated = true});
    auto root = data_[0].get();
    auto last = root;

    for (auto current = word.begin(); current != word.end(); ++current) {
      data_.emplace_back(new Node{.longest_length = static_cast<size_t>(std::distance(word.begin(), current)) + 1});
      auto new_longest_node = data_.back().get();

      size_t idx = ConcreteAlphabet::GetIndex(*current);

      while (last != nullptr && To(last, idx) == nullptr) {
        To(last, idx) = new_longest_node;
        last = last->link;
      }

      if (last == nullptr) {
        new_longest_node->link = root;
        last = new_longest_node;
        continue;
      }

      auto node_through_current = To(last, idx);
      if (last->longest_length + 1 == node_through_current->longest_length) {
        new_longest_node->link = node_through_current;
        last = new_longest_node;
        continue;
      }

      data_.emplace_back(new Node(*node_through_current));
      auto clone = data_.back().get();
      clone->link = node_through_current->link;
      node_through_current->link = clone;
      clone->longest_length = last->longest_length + 1;
      new_longest_node->link = clone;

      while (last != nullptr && To(last, idx) == node_through_current) {
        To(last, idx) = clone;
        last = last->link;
      }

      last = new_longest_node;
    }

    while (last != nullptr) {
      last->is_terminated = true;
      last = last->link;
    }
  }

  template <Word<T> ConcreteWord = std::string>
  constexpr bool IsSuffix(const ConcreteWord& word) const {
    auto current_node = data_[0].get();
    for (auto&& symbol : word) {
      if (current_node == nullptr) {
        return false;
      }
      current_node = To(current_node, ConcreteAlphabet::GetIndex(symbol));
    }

    return current_node != nullptr && current_node->is_terminated;
  }

 private:
  Node*& To(Node*& node, size_t idx) {
    return node->to[idx];
  }

  Node* To(Node* node, size_t idx) const {
    if constexpr (HasContaines<decltype(node->to), std::size_t>) {
      if (node->to.contains(idx)) {
        return node->to[idx];
      }
      return nullptr;
    } else {
      return node->to[idx];
    }
  }

  struct Node {
    size_t longest_length = 0;
    Node* link = nullptr;
    Container<Node*> to = Container<Node*>();
    bool is_terminated = false;
  };

  std::vector<std::unique_ptr<Node>> data_;
};
