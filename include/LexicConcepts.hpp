#include <concepts>
#include <iterator>

template <typename T, typename U>
concept Alphabet = requires(std::size_t index, U character) {
  { T::GetSymbol(index) } -> std::same_as<U>;
  { T::GetIndex(character) } -> std::same_as<std::size_t>;
  { T::Size() } -> std::same_as<std::size_t>;
};

template <typename T, typename U>
concept Word = requires(const T word) {
  { word.begin() } -> std::forward_iterator;
  { word.end() } -> std::forward_iterator;
  { *(word.begin()) } -> std::same_as<const U&>;
};

struct EnglishAlphabet {
  static constexpr std::size_t Size() {
    return 26;
  }

  static constexpr char GetSymbol(std::size_t idx) {
    return static_cast<char>('a' + idx);
  }

  static constexpr std::size_t GetIndex(char symbol) {
    return symbol - 'a';
  }
};

template <typename T, typename U>
concept HasContaines = requires(T container, U inner) { container.contains(inner); };

static_assert(Alphabet<EnglishAlphabet, char>);
static_assert(Word<std::string, char>);
