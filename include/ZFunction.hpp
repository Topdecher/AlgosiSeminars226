#include <vector>
#include <functional>
#include <iterator>

template <class T, class Compare = std::equal_to<T>>
class ZetFunction {
 public:
  template <std::random_access_iterator Iter>
  ZetFunction(Iter begin, Iter end, Compare compare = Compare()) : compare_(compare), z_(std::distance(begin, end)) {
    size_t left_border = 0;
    size_t right_border = 0;
    for (size_t i = 1; i < z_.size(); ++i) {
      if (i <= right_border) {
        z_[i] = std::min(right_border - i + 1, z_[i - left_border]);
      }
      while ((i + z_[i] < z_.size()) && (compare_(begin[z_[i]], begin[i + z_[i]]))) {
        ++z_[i];
      }
      if (i + z_[i] - 1 > right_border) {
        left_border = i;
        right_border = i + z_[i] - 1;
      }
    }
  }

  constexpr size_t operator[](size_t idx) const {
    return z_[idx];
  }

 private:
  Compare compare_;
  std::vector<size_t> z_;
};

template <std::random_access_iterator Iter>
ZetFunction(Iter, Iter) -> ZetFunction<typename std::iterator_traits<Iter>::value_type>;
