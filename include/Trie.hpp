#include <array>
#include <map>
#include <unordered_map>
#include <string>
#include "LexicConcepts.hpp"

template <typename T, template <typename> typename Container, Alphabet<T> ConcreteAlphabet = EnglishAlphabet>
class Trie {
 public:
  Trie() : root_(new Node()) {
  }

  template <Word<T> ConcreteWord = std::string>
  bool HasWord(const ConcreteWord& word) const {
    Node* current_node = root_.get();
    for (auto&& symbol : word) {
      if (current_node == nullptr) {
        return false;
      } else {
        current_node = [&]() -> Node* {
          if constexpr (HasContaines<decltype(current_node->to), std::size_t>) {
            if (current_node->to.contains(ConcreteAlphabet::GetIndex(symbol))) {
              return current_node->to[ConcreteAlphabet::GetIndex(symbol)].get();
            }
            return nullptr;
          } else {
            return current_node->to[ConcreteAlphabet::GetIndex(symbol)].get();
          }
        }();
      }
    }

    return current_node != nullptr && current_node->is_terminated;
  }

  template <Word<T> ConcreteWord = std::string>
  void AddWord(const ConcreteWord& word) {
    Node* current_node = root_.get();
    for (auto&& symbol : word) {
      size_t index = ConcreteAlphabet::GetIndex(symbol);

      auto is_containes = [&]() {
        if constexpr (HasContaines<decltype(current_node->to), std::size_t>) {
          return current_node->to.contains(index);
        } else {
          return current_node->to[index] != nullptr;
        }
      }();

      if (!is_containes) {
        current_node->to[index] = std::make_unique<Node>();
      }

      current_node = current_node->to[index].get();
    }
    current_node->is_terminated = true;
  }

 private:
  struct Node {
    Node() : is_terminated(false), to() {
    }

    bool is_terminated;
    Container<std::unique_ptr<Node>> to;
  };

  std::unique_ptr<Node> root_;
};
