#!/bin/bash

set -e

TARGET="$1"

./run.sh $TARGET --debug

./clear.sh
./build.sh $TARGET
valgrind --leak-check=full --show-leak-kinds=all ./bin/${TARGET}.out
