FROM ubuntu:latest

WORKDIR /Algosi

COPY requirements_ubuntu.sh .
RUN bash requirements_ubuntu.sh

COPY build.sh .
COPY clear.sh .
COPY run.sh .
COPY test.sh .

COPY CMakeLists.txt .

COPY include ./include
COPY test ./test

ENTRYPOINT ["/Algosi/test.sh"]
